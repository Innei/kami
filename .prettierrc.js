/*
 * @Author: Innei
 * @Date: 2020-09-17 14:02:24
 * @LastEditTime: 2021-08-28 16:46:20
 * @LastEditors: Innei
 * @FilePath: /web/.prettierrc.js
 * Mark: Coding with Love
 */
module.exports = require('@innei-util/prettier')
